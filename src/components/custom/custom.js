import React, {useState} from "react";
import {DropDown, DropDownContainer, ScrollableSection} from './styledComponents'
import {MockCheckBoxes} from './mockCheckBoxes'
import { MockSearchBar } from "./mockSearchBar";

export const CustomDropDown = ({checkboxValues}) => {

    const [isActive, setIsActive] = useState(false)
    const [checkedOptions, setCheckedOptions] = useState([])
    const [searchArr, setSearchArr] = useState(checkboxValues)

    const toggleVisibility = () => {
        isActive? setIsActive(false) : setIsActive(true)
    }

    const handleChecboxSelection = (event) => {
        const selected = event.target.value
        let selectedArr = checkedOptions

        if(selectedArr.includes(selected)){
            selectedArr = selectedArr.filter(el => el !== selected)
        }else{
            selectedArr = [...new Set([...checkedOptions, selected])]
        }

        setCheckedOptions(selectedArr)
    }

    const handleClear = () => {
        setCheckedOptions([])
    }

    const handleSearch = (event) => {
        const searchString = event.target.value
        const regexRule = new RegExp(`${searchString}`, 'i')
        const matchArr = checkboxValues.filter(val => regexRule.test(val))

        setSearchArr(matchArr)
    }

    return (
        <DropDown>
            <p onClick={toggleVisibility} style={{cursor: 'pointer'}}>Click Me</p>

            <DropDownContainer isActive={isActive} heightInPixels={300}>
                <MockSearchBar handleChange={handleSearch}/>

                <ScrollableSection>
                    <MockCheckBoxes options={searchArr} selected={checkedOptions} handleChange={handleChecboxSelection}/>     
                </ScrollableSection>

                <div style={{display:"block"}}>
                    <p style={{display:"inline"}}> {checkedOptions.length} selected</p>
                    <button style={{display:"inline"}} onClick={handleClear}>Clear</button>
                </div>
            </DropDownContainer>

        </DropDown>
    )
}
