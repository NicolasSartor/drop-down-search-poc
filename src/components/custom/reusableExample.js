import React, {useState} from "react";

const DropDownStyle = {
    position: 'relative',
    zIndex: 1
}


const formDropDownContainerStyle = (isActive, height) => {
    let DropDownContainerStyle = {
        overflow: 'hidden',
        position: 'absolute',
        left: '0',
        top: 'calc(100% + 5px)',
        pointerEvents: 'none',
        backgroundColor: 'white',
        padding: '5px',
        borderRadius: '5px',
        boxShadow: '0 2px 5px 0 rgba(0, 0, 0, 0.1)',
        transition: 'opacity 250ms ease-in-out',
        opacity: '0',
        width: '300px',
        height: 'auto'
    }
    if(isActive){
        DropDownContainerStyle.opacity = '1'
        DropDownContainerStyle.pointerEvents = 'auto'
    }
    if(height){
        DropDownContainerStyle.height = height
    }

    return DropDownContainerStyle
}

export const CustomDropDownAgnostic = ({ToggleComponent, MenuComponent}) => {

    const [isActive, setIsActive] = useState(false)

    const toggleVisibility = () => {
        isActive? setIsActive(false) : setIsActive(true)
    }

    return (
        <div style={DropDownStyle}>
            <div onClick={toggleVisibility} style={{cursor: 'pointer'}}>
                <ToggleComponent/>
            </div>

            <div style={formDropDownContainerStyle(isActive)}>
                <MenuComponent/>
            </div>

        </div>
    )
}
