import * as React from "react";
import MenuItem from "@mui/material/MenuItem";
import ListItemText from "@mui/material/ListItemText";
import Checkbox from "@mui/material/Checkbox";



export function MockCheckBoxes({ handleChange, options, selected }) {
 

  return (
    <>
      {options.map((option) => (
        <MenuItem key={option} value={option}>
          <Checkbox onChange={handleChange} checked={selected.indexOf(option) > -1} value={option} />
          <ListItemText primary={option} />
        </MenuItem>
      ))}
    </>
  );
}
