import * as React from "react";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";

export const MockSearchBar = ({value, handleChange}) => {
  return (
    <>
      <Stack
        component="form"
        sx={{
          width: "25ch",
        }}
        spacing={2}
        noValidate
        autoComplete="off"
      >
        <TextField
          hiddenLabel
          id="filled-hidden-label-small"
          label="Search"
          onChange={handleChange}
          value={value}
        />
      </Stack>
    </>
  );
};
