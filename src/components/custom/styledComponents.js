import React from "react";
import styled from "styled-components";

export const DropDown = styled.div`
  position: relative;
  z-index: 3;
`;

// top: calc(100% + 5px); gives 5px of distance between "click me" and DropDownContainer
// overflow: hidden; prevents the scrolling of the whole component, so we can delegate the scrolling to "ScrollableSection"
export const DropDownContainer = styled.div`
  overflow: hidden;
  position: absolute;
  left: 0;
  top: calc(100% + 5px);
  pointer-events: none;

  background-color: white;
  padding: 5px;
  border-radius: 5px;
  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.1);
  transition: opacity 250ms ease-in-out;
  opacity: 0;

  width: 300px;

  ${({isActive, heightInPixels}) =>
  `${
        heightInPixels
        ? `height: ${heightInPixels}px;`
        : "height: 400px;"
    }
    ${
      isActive &&
      `
        opacity: 1;
        pointer-events: auto;
      `
    }
    `};
`;

export const ScrollableSection = styled.div`
  margin-top: 5px;
  height: 60%;
  overflow-y: auto;
  margin-bottom: 5px;
`;
