import React, {useState, useEffect} from "react";
import {MockCheckBoxes} from './mockCheckBoxes'
import { MockSearchBar } from "./mockSearchBar";

const DropDownStyle = {
    position: 'relative',
    zIndex: 2
}


const formDropDownContainerStyle = (isActive, height) => {
    let DropDownContainerStyle = {
        overflow: 'hidden',
        position: 'absolute',
        left: '0',
        top: 'calc(100% + 5px)',
        pointerEvents: 'none',
        backgroundColor: 'white',
        padding: '5px',
        borderRadius: '5px',
        boxShadow: '0 2px 5px 0 rgba(0, 0, 0, 0.1)',
        transition: 'opacity 250ms ease-in-out',
        opacity: '0',
        width: '300px',
        height: '400px'
    }
    if(isActive){
        DropDownContainerStyle.opacity = '1'
        DropDownContainerStyle.pointerEvents = 'auto'
    }
    if(height){
        DropDownContainerStyle.height = height
    }

    return DropDownContainerStyle
}

const ScrollableSectionStyle = {
    marginTop: '5px',
    height: '60%',
    overflowY: 'auto',
    marginBottom: '5px',
}


export const CustomDropDownJSX = ({checkboxValues, onChange = (arr)=>(console.log('Values Arr', arr))}) => {

    const [isActive, setIsActive] = useState(false)
    const [checkedOptions, setCheckedOptions] = useState([])
    const [searchArr, setSearchArr] = useState(checkboxValues)

    useEffect(()=>{
        onChange(checkedOptions)
    },[checkedOptions])

    const toggleVisibility = () => {
        isActive? setIsActive(false) : setIsActive(true)
    }

    const handleChecboxSelection = (event) => {
        const selected = event.target.value
        let selectedArr = checkedOptions

        if(selectedArr.includes(selected)){
            selectedArr = selectedArr.filter(el => el !== selected)
        }else{
            selectedArr = [...new Set([...checkedOptions, selected])]
        }

        setCheckedOptions(selectedArr)
    }

    const handleClear = () => {
        setCheckedOptions([])
    }

    const handleSearch = (event) => {
        const searchString = event.target.value
        const regexRule = new RegExp(`${searchString}`, 'i')
        const matchArr = checkboxValues.filter(val => regexRule.test(val))

        setSearchArr(matchArr)
    }

    return (
        <div style={DropDownStyle}>
            <p onClick={toggleVisibility} style={{cursor: 'pointer'}}>Click Me</p>

            <div style={formDropDownContainerStyle(isActive, 300)}>
                <MockSearchBar handleChange={handleSearch}/>

                <div style={ScrollableSectionStyle}>
                    <MockCheckBoxes options={searchArr} selected={checkedOptions} handleChange={handleChecboxSelection}/>     
                </div>

                <div style={{display:"block"}}>
                    <p style={{display:"inline"}}> {checkedOptions.length} selected</p>
                    <button style={{display:"inline"}} onClick={handleClear}>Clear</button>
                </div>
            </div>

        </div>
    )
}
