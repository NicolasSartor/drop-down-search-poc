import logo from './logo.svg';
import './App.css';
// import {ReactSelectSearch, CustomSelect, options} from './components/react-select-search'
// import {CustomSelectMUI} from './components/material-ui'
import {CustomDropDown, CustomDropDownJSX, CustomDropDownAgnostic} from './components/custom'

const ExampleToggle = () => <img style={{width: 200}} src='https://i.kym-cdn.com/entries/icons/mobile/000/026/638/cat.jpg' alt=""/>
const ExampleMenu = () => <div>{Array(5).fill(1).map(el => <p>list</p>)}</div>
 
const checkboxValues = [ // would come in as props
  "Oliver Hansen",
  "Van Henry",
  "April Tucker",
  "Ralph Hubbard",
  "Omar Alexander",
  "Carlos Abbott",
  "Miriam Wagner",
  "Bradley Wilkerson",
  "Virginia Andrews",
  "Kelly Snyder",
];

function App() {
  return (
  <div>
    <h1>Drop Down Search Component POC</h1>
    {/* <ReactSelectSearch/>
    <CustomSelect options={options}/> */}
    {/* <CustomSelectMUI/> */}
    <h2>Styled Components:</h2>
    <CustomDropDown checkboxValues={checkboxValues}/>
    <h2>Pure JSX:</h2>
    <CustomDropDownJSX checkboxValues={checkboxValues}/>

    <h2>Reusable Agnostic DropDown</h2>
    <CustomDropDownAgnostic ToggleComponent={ExampleToggle} MenuComponent={ExampleMenu}/>

  </div>
  );
}

export default App;
